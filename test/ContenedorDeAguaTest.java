

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class ContenedorDeAguaTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class ContenedorDeAguaTest
{
    @Test
    public void testLlenarContenedorMenosAguaTotal()
    {
        ContenedorDeAgua contenedor = new ContenedorDeAgua(5);
        boolean salida = contenedor.llenarContenedor(1);
        assertEquals(true, salida);
    }
    
    @Test
    public void testLlenarContenedorMismaCantidadTotal()
    {
        ContenedorDeAgua contenedor = new ContenedorDeAgua(3);
        boolean salida = contenedor.llenarContenedor(3);
        assertEquals(true, salida);
    }
    
    @Test
    public void testLlenarContenedorMasAguaTotal()
    {
        ContenedorDeAgua contenedor = new ContenedorDeAgua(4);
        boolean salida = contenedor.llenarContenedor(6);
        assertEquals(false, salida);
    }
    
    @Test
    public void testLlenarContenedorSinAgua()
    {
        ContenedorDeAgua contenedor = new ContenedorDeAgua(4);
        boolean salida = contenedor.llenarContenedor(0);
        assertEquals(false, salida);
    }
}

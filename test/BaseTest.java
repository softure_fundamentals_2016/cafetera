

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class BaseTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class BaseTest
{
    @Test
    public void testPrenderBase()
    {
        Base base = new Base();
        boolean salir = base.prender();
        assertTrue(salir);
    }
}

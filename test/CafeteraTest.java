

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class CafeteraTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class CafeteraTest
{
    @Test
    public void testHacerCafe()
    {
        Cafetera cafetera = new Cafetera(5, 5);
        boolean salida = cafetera.hacerCafe(1, 1);
        assertEquals(true, salida);
    }
    
    @Test
    public void testHacerCafeMuchaAgua()
    {
        Cafetera cafetera = new Cafetera(5, 5);
        boolean salida = cafetera.hacerCafe(6, 1);
        assertEquals(false, salida);
    }
    
    @Test
    public void testHacerCafeSinAgua()
    {
        Cafetera cafetera = new Cafetera(5, 5);
        boolean salida = cafetera.hacerCafe(0, 1);
        assertEquals(false, salida);
    }
    
    @Test
    public void testHacerCafeSinCafe()
    {
        Cafetera cafetera = new Cafetera(5, 5);
        boolean salida = cafetera.hacerCafe(1, 0);
        assertEquals(false, salida);
    }
    
    @Test
    public void testHacerCafeMasAguaQueCafe()
    {
        Cafetera cafetera = new Cafetera(5, 5);
        boolean salida = cafetera.hacerCafe(2, 1);
        assertEquals(false, salida);
    }
    
    @Test
    public void testHacerCafeMenosAguaQueCafe()
    {
        Cafetera cafetera = new Cafetera(5, 5);
        boolean salida = cafetera.hacerCafe(1, 3);
        assertEquals(false, salida);
    }
}

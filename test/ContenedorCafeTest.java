

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class ContenedorCafeTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class ContenedorCafeTest
{
    @Test
    public void testLlenarContenedorCafe()
    {
        ContenedorCafe contenedor = new ContenedorCafe(4);
        boolean salida = contenedor.llenarContenedor(1);
        assertEquals(true, salida);
    }
}

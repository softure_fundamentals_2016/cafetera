
/**
 * Es la clase principal, y que prepara tazas de cafe.
 * 
 * @author 
 */
public class Cafetera
{
    /** El contenedor de Agua */
    private ContenedorDeAgua contenedorAgua;
    
    /** El contenedor de filtros */
    private Filtro contenedorFiltro;
    
    /** El contenedor de cafe a ser filtrado */
    private ContenedorCafe contenedorCafe;
    
    /** la Base de la cafetera */
    private Base base;
    
    /**
     * Constructor de objectos de la clase Cafetera
     */
    public Cafetera(int capacidadAgua, int capacidadCafePolvo)
    {
        contenedorAgua = new ContenedorDeAgua(capacidadAgua);
        contenedorFiltro = new Filtro();
        contenedorCafe = new ContenedorCafe(capacidadCafePolvo);
        base = new Base();
    }

    /**
     * Devuelve el estado true, indicando que la taza de cafe fue servida con exito.
     *
     * @return true si cafe esta servido.
     */
    public boolean hacerCafe(int tazasAgua, int cafePolvo)
    {
        boolean aguaLista = llenarContenedorAgua(tazasAgua);
        boolean filtroListo = ponerFiltro();
        boolean cafeListo = llenarContenedorCafe(cafePolvo);
        boolean baseLista = prenderBase();
        boolean aguaIgualCafe = false;
        
        if(tazasAgua == cafePolvo)
        {
            aguaIgualCafe = true;
        }
        
        boolean cafe = aguaLista && filtroListo && cafeListo
                    && baseLista && aguaIgualCafe;
        return cafe;
    }

    /**
     * Delega ha la clase ContenedorAgua para que indique si la cantidad de tazas esta lista. 
     * @param numero de tazas de agua provistas al contenedor
     * 
     * @return true si el contenedor de agua esta listo
     */
    private boolean llenarContenedorAgua(int tazasAgua)
    {
        return contenedorAgua.llenarContenedor(tazasAgua);
    }

    /**
     * Delega ha la clase ContenedorFiltro indicar si esta listo para ser usado.
     * 
     * @return true si el filtro esta listo
     */
    private boolean ponerFiltro()
    {
        return contenedorFiltro.ponerFiltro();
    }

    /**
     * Delega ha la clase ContenedorCafe para que indique si el cafe ya fue colocado. 
     * @param porcionesCafe numero de tazas de agua provistas al contenedor
     * 
     * @return true si el contenedor de cafe esta listo
     */
    private boolean llenarContenedorCafe(int porcionesCafe)
    {
        return contenedorCafe.llenarContenedor(porcionesCafe);
    }

    /**
     * Delega ha la clase Base para que indique si esta prendida. 
     * 
     * @return true si la base esta prendida
     */
    private boolean prenderBase()
    {
        return base.prender();
    }
}

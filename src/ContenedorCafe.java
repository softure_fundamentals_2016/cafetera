
/**
 * Representa el Contenedor de cafe a ser filtrado
 * 
 * @author 
 */
public class ContenedorCafe
{
    /** cantidad maxima de porsiones de cafe */
    private int cantidadTotal;

    /**
     * Constructor for objects of class ContenedorCafe
     */
    public ContenedorCafe(int cantidadTotal)
    {
        this.cantidadTotal = cantidadTotal;
    }

    /**
     * llena el contenedor de cafe, e indica si la cantidad dispensada
     * supera la cantidad total del contenedor.
     * 
     * @param  cantidadCafe la cantidad de cafe provista
     * @return     true si la porcion de cafe provista es menor igual
     * que la cantidad total.
     */
    public boolean llenarContenedor(int cantidadCafe)
    {
        if(cantidadCafe > 0 && cantidadCafe <= cantidadTotal)
        {
            return true;
        }
        return false;
    }
}

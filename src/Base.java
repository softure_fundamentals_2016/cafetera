
/**
 * Representa la Base de una cafetera.
 * 
 * @author
 */
public class Base
{
    private boolean estado;

    /**
     * Constructor para crear objetos de la clase Base.
     */
    public Base()
    {
        estado = false;
    }

    /**
     * Prende la base de la cafetera
     * 
     * @return  true si la case fue prendida
     */
    public boolean prender()
    {
        estado = true;
        return estado;
    }
}

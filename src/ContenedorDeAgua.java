
/**
 * Representa la jarra que contiene el agua de la cafetera.
 * 
 * @author
 */
public class ContenedorDeAgua
{
    /** la cantidad maxima de agua que soporta la jarra*/
    private int cantidadTotal;

    public ContenedorDeAgua(int cantidadTotal)
    {
        this.cantidadTotal = cantidadTotal;
    }
    
    /**
     * Devuelve true si la cantidad de agua no supera el maximo aceptado.
     * 
     * @return true si la cantidad a sido llenada en la jarra.
     */
    public boolean llenarContenedor(int cantidad)
    {
        if(cantidad > 0 && cantidad <= cantidadTotal)
        {
            return true;
        }
        return false;
    }
}

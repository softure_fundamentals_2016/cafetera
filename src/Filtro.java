
/**
 * Representa el filtro de papel que es insertado en la cafetera.
 * 
 * @author
 */
public class Filtro
{
    /** Filtro de papel */
    private boolean filtroPapel;

    /**
     * Constructor de la clase Filtro
     */
    public Filtro()
    {        
    }

    /**
     * Devuelve el estado true, representando que el filtro esta listo para se usado.
     *
     * @return true si el filtro esta listo para ser usado.
     */
    public boolean ponerFiltro()
    {
        filtroPapel = true;
        return filtroPapel;
    }
}
